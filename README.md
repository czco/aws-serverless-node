# SERVERLESS MONGO AWS CRUD API GATEWAY

This example demonstrates how to use a MongoDB with aws and serverless.

## Setup

```
npm install -g serverless
npm install
export AWS_ACCESS_KEY_ID=<your-key-here>
export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
serverless deploy
```

## Usage

In `handler.js` update the `mongoString` with your mongoDB url.

_Create_

```bash
curl -XPOST -H "Content-type: application/json" -d '{
   "name" : "Jane",
   "firstname" : "Doe",
   "city" : "San Francisco",
   "birth" : "02/28/2018"
}' 'https://<your-api-gateway-host>/dev/user/'
```

```json
{ "id": "<your-new-record-id>" }
```

_READ_

```bash
curl -XGET -H "Content-type: application/json" 'https://<your-api-gateway-host>/dev/user/<your-new-record-id>'
```

```json
[
  {
    "_id": "<your-new-record-id>",
    "name": "Jane",
    "firstname": "Doe",
    "birth": "02/28/2018",
    "city": "San Francisco",
    "ip": "",
    "__v": 0
  }
]
```

_UPDATE_

```bash
curl -XPUT -H "Content-type: application/json" -d '{
   "name" : "Jacobo",
   "firstname" : "Smith",
   "city" : "Chicago",
   "birth" : "01/01/2000"
}' 'https://<your-api-gateway-host>/dev/user/<your-new-record-id>'
```

```json
"Ok"
```

_DELETE_

```bash
curl -XDELETE -H "Content-type: application/json" 'https://<your-api-gateway-host>/dev/user/<your-new-record-id>'
```

```json
"Ok"
```
